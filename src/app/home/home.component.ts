﻿import { Component } from '@angular/core';

import { UserService, AuthenticationService } from '@app/_services';
import { Operation } from '@app/_models/operation';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    loading = false;
    operations: Operation[];
    message: string;
    balance: number;

    constructor(private userService: UserService) { }

    ngOnInit() {
        this.loading = true;
     this.gethistory()
    }

    form = new FormGroup({
        operationType: new FormControl('', Validators.required),
        amount: new FormControl('', Validators.required)
      });
      
      get f(){
        return this.form.controls;
      }
      
      submit(){
          if(this.form.value.operationType =='deposit'){
              this.userService.postDeposit(this.form.value.amount).subscribe(resp=> 
               {console.log(resp)
                 this.message = resp.message;
                this.gethistory();
              });

          }else if(this.form.value.operationType =='withdrawal'){
            this.userService.postWithdrawal(this.form.value.amount).subscribe(resp=> 
             {   console.log(resp); this.message = resp.message;
                this.gethistory();
            });

          } 
      }

      gethistory(){
        this.userService.getHistory().subscribe(data => {
            this.operations = data;
            this.balance =data[0].balance;
        })
        this.loading = false;
      }
}