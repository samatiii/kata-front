// tslint:disable
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Pipe, PipeTransform, Injectable, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, Directive, Input, Output } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Observable, of as observableOf, throwError } from 'rxjs';

import { Component } from '@angular/core';
import { HomeComponent } from './home.component';
import { UserService } from '@app/_services';
import { HttpClientTestingModule } from '@angular/common/http/testing';

@Directive({ selector: '[oneviewPermitted]' })
class OneviewPermittedDirective {
  @Input() oneviewPermitted;
}

@Pipe({name: 'translate'})
class TranslatePipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({name: 'phoneNumber'})
class PhoneNumberPipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({name: 'safeHtml'})
class SafeHtmlPipe implements PipeTransform {
  transform(value) { return value; }
}

describe('HomeComponent', () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule, ReactiveFormsModule ,HttpClientTestingModule],
      declarations: [
        HomeComponent,
        TranslatePipe, PhoneNumberPipe, SafeHtmlPipe,
        OneviewPermittedDirective
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
      providers: [
        UserService
      ]
    }).overrideComponent(HomeComponent, {  

    }).compileComponents();
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.debugElement.componentInstance; 
    });

  afterEach(() => {
    component.ngOnDestroy = function() {};
    fixture.destroy();
  });

  it('should run #constructor()', async () => {
    expect(component).toBeTruthy(); 
  });

  it('should run GetterDeclaration #f', async () => {
    component.form = component.form || {};
    component.form.controls = 'controls';
    const f = component.f;

  });

  it('should run #ngOnInit()', async () => {
    spyOn(component, 'gethistory');
    component.ngOnInit();
    // expect(component.gethistory).toHaveBeenCalled();
  });

  it('should run #submit()', async () => {
    component.form = component.form || {};
    component.form.value = {
      operationType: {},
      amount: {}
    };
    component.userService = component.userService || {};
    spyOn(component.userService, 'postDeposit').and.returnValue(observableOf({
      message: {}
    }));
    spyOn(component.userService, 'postWithdrawal').and.returnValue(observableOf({
      message: {}
    }));
    spyOn(component, 'gethistory');
    component.submit();
    // expect(component.userService.postDeposit).toHaveBeenCalled();
    // expect(component.userService.postWithdrawal).toHaveBeenCalled();
    // expect(component.gethistory).toHaveBeenCalled();
  });

  it('should run #gethistory()', async () => {
    component.userService = component.userService || {};
    spyOn(component.userService, 'getHistory').and.returnValue(observableOf({}));
    component.gethistory();
    // expect(component.userService.getHistory).toHaveBeenCalled();
  });

});
