﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Operation } from '@app/_models/operation';
import { Result } from '@app/_models/result';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getHistory() {
        return this.http.get<[Operation]>(`${environment.apiUrl}/statement`);
    }

    postDeposit(amount: number){
        return this.http.post<Result>(`${environment.apiUrl}/deposit`, {
            amount: amount
          });
    }

    postWithdrawal(amount: number){
        return this.http.post<Result>(`${environment.apiUrl}/withdrawal`, {
            amount: amount
          });
    }

}