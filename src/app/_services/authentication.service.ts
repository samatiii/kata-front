﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private authenticated = false;
    private token: string;

    constructor(private http: HttpClient) {
    }

    isAuthenticated(): boolean {
        return this.authenticated;
    }
    getToken(): string{
        return this.token;
    }

    login(email: string, password: string) {
        return this.http.post<any>(`${environment.apiUrl}/client/signin`, { email, password })
            .pipe(map(data => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('token', data.accessToken);
                this.authenticated = true;
                this.token = data.accessToken;
                return data;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('token');
        this.authenticated = false;
    }
}