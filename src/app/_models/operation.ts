export class Operation {
    opId: number;
    amount: number;
    operationDate: Date;
    type: string;
    balance: number;
}