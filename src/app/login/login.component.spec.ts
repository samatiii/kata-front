// tslint:disable
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Pipe, PipeTransform, Injectable, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, Directive, Input, Output } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Observable, of as observableOf, throwError } from 'rxjs';

import { Component } from '@angular/core';
import { LoginComponent } from './login.component';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '@app/_services';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { findStaticQueryIds } from '@angular/compiler';
import { DOMHelper } from 'src/testing/dom-helper';

@Injectable()
class MockRouter {
  navigate = function() {};
}


describe('LoginComponent', () => {
  let fixture;
  let component;
  let domHelper : DOMHelper<LoginComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule, ReactiveFormsModule,HttpClientTestingModule ],
      declarations: [
        LoginComponent
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {url: 'url', params: {}, queryParams: {}, data: {}},
            url: observableOf('url'),
            params: observableOf({}),
            queryParams: observableOf({}),
            fragment: observableOf('fragment'),
            data: observableOf({})
          }
        },
        { provide: Router, useClass: MockRouter },
        AuthenticationService
      ]
    }).overrideComponent(LoginComponent, {

    }).compileComponents();
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.debugElement.componentInstance;
    domHelper = new DOMHelper(fixture)
  });

  afterEach(() => {
    component.ngOnDestroy = function() {};
    fixture.destroy();
  });

  describe('HTML tests',()=>{
    it('Should render title in a h4 tag',() => {
      expect(domHelper.singleText('h4')).toContain('Samati BANK')
    })
  })

  describe('Event Tests',()=>{
    it('should run #constructor()', async () => {
      expect(component).toBeTruthy();
    });

    it('should run GetterDeclaration #f', async () => {
      component.loginForm = component.loginForm || {};
      component.loginForm.controls = 'controls';
      const f = component.f;

    });

    it('should run #ngOnInit()', async () => {
      component.formBuilder = component.formBuilder || {};
      spyOn(component.formBuilder, 'group');
      component.route = component.route || {};
      component.route.snapshot = {
        queryParams: {
          'returnUrl': {}
        }
      };
      component.ngOnInit();
      // expect(component.formBuilder.group).toHaveBeenCalled();
    });

    it('should run #onSubmit()', async () => {
      component.loginForm = component.loginForm || {};
      component.loginForm.invalid = 'invalid';
      component.authenticationService = component.authenticationService || {};
      spyOn(component.authenticationService, 'login').and.returnValue(observableOf({}));
      // component.f = component.f || {};
      // component.f.email = {
      //   value: {}
      // };
      // component.f.password = {
      //   value: {}
      // };
      component.router = component.router || {};
      spyOn(component.router, 'navigate');
      component.onSubmit();
      // expect(component.authenticationService.login).toHaveBeenCalled();
      // expect(component.router.navigate).toHaveBeenCalled();
    });
})


});
