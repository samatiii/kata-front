import { ComponentFixture } from '@angular/core/testing';
import {By} from '@angular/platform-browser';

export class DOMHelper<T> {
    private fixture: ComponentFixture<T>;

    constructor(fixture: ComponentFixture<T>){
        this.fixture = fixture;
    }

    singleText(tagName: string): string{
        debugger
        const compiled = this.fixture.debugElement.query(By.css(tagName));
        if(compiled){
               return compiled.nativeElement.textContent;
        }

    }
}